//
//  WordScrambleApp.swift
//  WordScramble
//
//  Created by Marcel Mravec on 19.01.2022.
//

import SwiftUI

@main
struct WordScrambleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
