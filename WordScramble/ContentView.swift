//
//  ContentView.swift
//  WordScramble
//
//  Created by Marcel Mravec on 19.01.2022.
//

import SwiftUI

struct ContentView: View {
    @State private var usedWords = [String]()
    @State private var rootWord = ""
    @State private var newWord = ""
    
    @State private var errorTitle = ""
    @State private var errorMessage = ""
    @State private var showingError = false
    @State private var gameScore = 0
    
    
    var body: some View {
        NavigationView {
            List {
                Section {
                    TextField("Enter your word", text: $newWord)
                        .autocapitalization(.none)
                }

                Section {
                    ForEach(usedWords, id: \.self) { word in
                        HStack {
                            Image(systemName: "\(word.count).circle")
                            Text(word)
                            }
                    }
                }
                
                
                
            }
            .navigationTitle(rootWord)
            .onSubmit(addNewWord)
            .onAppear(perform: startGame)
            .alert(errorTitle, isPresented: $showingError) {
                Button("OK", role: .cancel) {}
            } message: {
                Text(errorMessage)
            }.toolbar {
                
                    ToolbarItem(placement: .navigationBarLeading) {
                        Text("Score: \(gameScore)")
                    }
                    ToolbarItem(placement: .navigationBarTrailing) {
                        Button("New Game", action: startGame)
                    }
                
            }
        }
    }
        
    
    
    func addNewWord() {
        let answer = newWord.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)

            // exit if the remaining string is empty
            guard answer.count > 0 else { return }
            guard isLongEnough(word: answer) else {
                wordError(title: "Word has bad length", message: answer == rootWord ? "You can't use rootWord" : "You can't use too short words")
            return
        }
            guard isOriginal(word: answer) else {
                wordError(title: "Word used already", message: "Be more original!")
                return
            }
            guard isPossible(word: answer) else {
                wordError(title: "Word not possible", message: "You can't spell this word from '\(rootWord)!'")
                return
            }
            guard isReal(word: answer) else {
                wordError(title: "Word not recognized", message: "You just can't make them up, you know! ")
                return
            }

        
            withAnimation {
                usedWords.insert(answer, at: 0)
                gameScore += answer.count * 2
            }
         
            newWord = ""
    }
    
    func startGame() {
        if let startWordsURL = Bundle.main.url(forResource: "start", withExtension: "txt") {
            if let startWords = try? String(contentsOf: startWordsURL) {
                let allWords = startWords.components(separatedBy: "\n")
                rootWord = allWords.randomElement() ?? "silkworm"
                gameScore = 0
                return
            }
        }
        fatalError("Couldn't load start.txt from Bundle.")
    }
    
    func isLongEnough(word: String) -> Bool {
        return word.count > 3 && word.count != rootWord.count
            
    }
    
    func isOriginal(word: String) -> Bool {
        !usedWords.contains(word)
    }
    
    func isPossible(word: String) -> Bool {
        var tempWord = rootWord
        for letter in word {
            if let pos = tempWord.firstIndex(of: letter) {
                tempWord.remove(at: pos)
            } else {
                return false
            }
        }
        return true
    }
    
    func isReal(word: String) -> Bool {
        let checker = UITextChecker()
        let range = NSRange(location: 0, length: word.utf16.count)
        let misspelledRange = checker.rangeOfMisspelledWord(in: word, range: range, startingAt: 0, wrap: false, language: "en")
        return misspelledRange.location == NSNotFound
    }
    
    func wordError(title: String, message: String) {
        errorTitle = title
        errorMessage = message
        showingError = true
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
